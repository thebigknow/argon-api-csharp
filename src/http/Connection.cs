using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace TBK.Argon.HTTP
{
    class Connection
    {
        private string _jwt;
        HttpClient client { get; set; }
        internal string jwt
        {
            get
            {
                return _jwt;
            }
            set
            {
                this._jwt = value;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", value);
            }
        }

        public Connection(string endpoint, string jwt = null)
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(endpoint);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            if (jwt != null)
            {
                this.jwt = jwt;
            }
        }

        public string getResource(string resource)
        {
            return doRequestAsync(resource: resource).GetAwaiter().GetResult();
        }

        public string postResource(string resource, string data)
        {
            return doRequestAsync(resource: resource, method: "POST", data: data).GetAwaiter().GetResult();
        }

        private async Task<string> doRequestAsync(string resource, string method = "GET", string data = null)
        {
            HttpResponseMessage response;
            switch (method)
            {
                case "POST":
                    StringContent content = new StringContent(data);
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    response = await client.PostAsync(resource, content);
                    break;
                default:
                    response = await client.GetAsync(resource);
                    break;
            }

            var body = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                return body;
            }
            else
            {
                if (response.Content.Headers.ContentType.MediaType == "application/vnd.api+json")
                {
                    throw new TBK.Argon.Errors.JsonApiException(body);
                }
                else
                {
                    // TODO: Exchange body for a message that states something about an invalid request or unknown error.
                    throw new TBK.Argon.Errors.Exception(body);
                }
            }
        }
    }
}