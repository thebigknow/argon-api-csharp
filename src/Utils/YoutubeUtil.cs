namespace TBK.Argon.Utils
{
    public class YoutubeUtil
    {
        private const string EmbedURL = "https://www.youtube.com/embed";

        public static string GetEmbedCode(string code)
        {
            var embedURL = $"{EmbedURL}/{code}?enablejsapi=1";
            return $"<iframe src=\"{embedURL}\" width=\"560\" height=\"315\" frameborder=\"0\" allowfullscreen></iframe>";
        }
    }
}