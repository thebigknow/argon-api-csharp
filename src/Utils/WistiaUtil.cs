using System.Collections.Generic;
using System.Web;
using Newtonsoft.Json;
using TBK.Argon.HTTP;

namespace TBK.Argon.Utils
{
    public class WistiaUtil
    {
        private const string OEmbedURL = "http://fast.wistia.com/oembed";
        private const string TBKMediaURL = "https://thebigknow.wistia.com/medias";

        public static string GetEmbedCode(string code)
        {
            var embedUrl = HttpUtility.UrlEncode($"{TBKMediaURL}/{code}?embedType=iframe&videoFoam=true&videoWidth=640");
            var client = new Connection(OEmbedURL);
            var jsonStr = client.getResource($"?url={embedUrl}");
            var data = JsonConvert.DeserializeObject<Dictionary<string,string>>(jsonStr);
            return data["html"];
        }
    }
}