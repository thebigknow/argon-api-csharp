using System;

namespace TBK.Argon.Errors
{
    [Serializable()]
    public class JsonApiException : Exception
    {
        public JsonApiException() : base() { }
        public JsonApiException(string message) : base(message) { }
        public JsonApiException(string message, System.Exception inner) : base(message, inner) { }

        protected JsonApiException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }

        private TBK.Argon.Models.Errors _errors;
        public TBK.Argon.Models.Errors errors
        {
            get
            {
                if (_errors == null) {
                    var helper = new TBK.Argon.JsonApiHelper();
                    this._errors = helper.DeserializeObject<TBK.Argon.Models.Errors>(Message);
                }
                return _errors;
            }
        }
    }
}