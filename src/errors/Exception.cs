using System;

namespace TBK.Argon.Errors
{
    [Serializable()]
    public class Exception : System.Exception
    {
        public Exception() : base() { }
        public Exception(string message) : base(message) { }
        public Exception(string message, System.Exception inner) : base(message, inner) { }

        protected Exception(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}