using System.Collections.Generic;

using TBK.Argon.Models;
using TBK.Argon.HTTP;
namespace TBK.Argon
{
    public class Client
    {
        public static Client authenticate(string endpoint, string identifier, string password, string academy)
        {
            var client = new Client(endpoint, null);
            client.authenticate(identifier, password, academy);
            return client;
        }
        private Connection connection;
        private JsonApiHelper jsonApiHelper;

        public Client(string endpoint, string jwt)
        {
            connection = new Connection(endpoint: endpoint, jwt: jwt);
            jsonApiHelper = new JsonApiHelper();
        }
        public List<Course> getCourses(string include = "lessons.parts.slides.activity")
        {
            var url = "courses";
            url = string.IsNullOrEmpty(include) ? url : $"{url}?include={include}";
            string courseJson = connection.getResource(url);
            return jsonApiHelper.DeserializeObject<List<Course>>(courseJson);
        }
        public Course getCourse(string identifier, string include = "lessons.parts.slides.activity")
        {
            var url = $"courses/{identifier}";
            url = string.IsNullOrEmpty(include) ? url : $"{url}?include={include}";
            string courseJson = connection.getResource(url);
            return jsonApiHelper.DeserializeObject<Course>(courseJson);
        }

        public void authenticate(string identifier, string password, string academy)
        {
            User u = new User()
            {
                Identifier = identifier,
                Password = password,
                Subdomain = academy
            };
            string ustring = jsonApiHelper.SerializeObject(u);
            var res = connection.postResource("session.json", ustring);
            u = jsonApiHelper.DeserializeObject<User>(res);
            connection.jwt = u.JwtToken;
        }
    }
}