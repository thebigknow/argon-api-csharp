using System.Collections.Generic;
using JsonApiSerializer;
using JsonApiSerializer.ContractResolvers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using TBK.Argon.Models;
using TBK.Argon.Models.Activities.V1;
using TBK.Argon.Utils;

namespace TBK.Argon
{
    public class JsonApiHelper
    {
        private JsonApiSerializerSettings _settings;
        private JsonApiSerializerSettings serializerSettings
        {
            get
            {
                if (_settings == null)
                {
                    _settings = new JsonApiSerializerSettings()
                    {
                        ContractResolver = new JsonApiContractResolver()
                        {
                            NamingStrategy = new SnakeCaseNamingStrategy()
                        }
                    };
                    var activityTypeMaps = new Dictionary<string, System.Type>() {
                        { Fullscreen.Type, typeof(Fullscreen) },
                        { Poll.Type, typeof(Poll) },
                        { Video.Type, typeof(Video) }
                    };
                    _settings.Converters.Add(new SubclassResourceObjectConverter<Activity>(activityTypeMaps));
                }
                return _settings;
            }
            set
            {
                this._settings = value;
            }
        }

        public T DeserializeObject<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json, serializerSettings);
        }

        public string SerializeObject<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj, serializerSettings);
        }
    }
}