using System.Collections.Generic;

namespace TBK.Argon.Models
{
    public class Part
    {
        public const string Type = "parts";
        public string Id { get; set; }
        public string Identifier { get; set; }
        public string Headline { get; set; }
        public string Subheadline { get; set; }
        public int Duration { get; set; }

        /* ----- Relationships ----- */
        public List<Slide> Slides { get; set; }
    }
}