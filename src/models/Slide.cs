using Newtonsoft.Json;
using TBK.Argon.Models.Activities.V1;

namespace TBK.Argon.Models
{
    public class Slide
    {
        public const string Type = "slides";
        public string Id { get; set; }
        public string Identifier { get; set; }

        /* ----- Relationships ----- */

        public Activity Activity { get; set; }
    }
}