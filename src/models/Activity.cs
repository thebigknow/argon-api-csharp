namespace TBK.Argon.Models
{
    public class Activity
    {
        public string Id { get; set; }
        public string Label { get; set; }
        public string BackgroundImage { get; set; }
    }
}