using System.Collections.Generic;
using JsonApiSerializer.JsonApi;
using Newtonsoft.Json;

namespace TBK.Argon.Models
{
    public class Course
    {
        public const string Type = "courses";
        public string Id { get; set; }
        public string Identifier { get; set; }
        public int Position { get; set; }
        public string Title { get; set; }
        public string CardIntro { get; set; }
        public string OverviewIntro { get; set; }
        public string TrailerCTA { get; set; }
        public string CardImage { get; set; }
        public string CardImageUrl { get; set; }
        public string ResultImage { get; set; }
        public string ResultImageUrl { get; set; }
        public Dictionary<string, string> Trailer { get; set; }

        /* ----- Relationships ----- */
        public List<Lesson> Lessons { get; set; }
    }
}