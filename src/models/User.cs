namespace TBK.Argon.Models
{
    public class User
    {
        public const string type = "users";
        public string Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JwtToken { get; set; }

        // Used for creating the initial session
        public string Identifier { get; set; }
        public string Password { get; set; }
        public string Subdomain { get; set; }
    }
}