using System.Collections.Generic;

namespace TBK.Argon.Models
{
    public class Errors
    {
        public List<Error> errors;
    }

    public class Error
    {
        public Dictionary<string, string> source { get; set; }
        public string detail { get; set; }
    }
}