using System.Collections.Generic;
//using Newtonsoft.Json;

namespace TBK.Argon.Models
{
    public class Lesson
    {
        public const string Type = "lessons";
        public string Id { get; set; }
        public string Identifier { get; set; }
        public int Position { get; set; }
        public string Locale { get; set; }
        public Dictionary<string, string> SupportedLanguages { get; set; }
        public string Title { get; set; }
        public Dictionary<string, string> Trailer { get; set; }
        public string CardIntro { get; set; }
        public string CardDescription { get; set; }
        public string OverviewIntro { get; set; }
        public string OverviewDescription { get; set; }

        /* ----- Relationships ----- */
        public List<Part> Parts { get; set; }
    }
}