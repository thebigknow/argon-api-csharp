namespace TBK.Argon.Models.Activities.V1
{
    public class Fullscreen : TBK.Argon.Models.Activity
    {
        public const string Type = "activity_v1_fullscreens";
        public string Headline { get; set; }
        public string Subheadline { get; set; }

    }
}
