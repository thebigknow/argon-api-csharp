namespace TBK.Argon.Models.Activities.V1
{
    public class Video : TBK.Argon.Models.Activity
    {
        public const string Type = "activity_v1_videos";
        public string Headline { get; set; }
        public string Provider { get; set; }
        public string Code { get; set; }
        public int CompleteThreshold { get; set; }

        public string GetEmbedCode()
        {
            switch(Provider)
            {
                case "wistia":
                    return TBK.Argon.Utils.WistiaUtil.GetEmbedCode(Code);
                case "youtube":
                    return TBK.Argon.Utils.YoutubeUtil.GetEmbedCode(Code);
                case "embed":
                    return Code;
                default:
                    return null;
            }
        }
    }
}