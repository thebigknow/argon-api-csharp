namespace TBK.Argon.Models.Activities.V1
{
    public class Poll : TBK.Argon.Models.Activity
    {
        public const string Type = "activity_v1_polls";
        public string Headline { get; set; }
        public string Prompt { get; set; }

    }
}